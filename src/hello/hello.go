package main

import (
	"fmt"
	"log"

	"bitbucket.org/onlinejudge95-lab/custom_module/src/greetings"
)

func main() {
	log.SetPrefix("greetings: ")
	log.SetFlags(0)

	message, err := greetings.Hello("Mayank")

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(message)
}
